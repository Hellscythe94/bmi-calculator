package wolowiec.michal.lab1.Model.DataGrabbers.Retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import wolowiec.michal.lab1.Model.CoronaDataPojo.CoronaStats;

public interface CoronaAPI {

    @GET("free-api")
    Call<CoronaStats> getGlobalStats(@Query("global") String stats);
}
