package wolowiec.michal.lab1.Model.CoronaDataPojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CoronaStats {

    @SerializedName("results")
    @Expose
    private List<Result> results = null;
    @SerializedName("stat")
    @Expose
    private String stat;

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

}