package wolowiec.michal.lab1.Model.DataGrabbers.Retrofit;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import wolowiec.michal.lab1.Model.CoronaDataPojo.CoronaStats;
import wolowiec.michal.lab1.View.Fragments.ChartFragment;

public class CoronaStatsGrabber implements Callback<CoronaStats> {

    static final String BASE_URL = "https://api.thevirustracker.com/";
    private CoronaStats coronaStats;
    private ChartFragment fragment;

    public CoronaStatsGrabber() {
    }

    public CoronaStatsGrabber(ChartFragment fragment) {
        this.fragment = fragment;
    }

    public void start(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        CoronaAPI coronaAPI = retrofit.create(CoronaAPI.class);

        Call<CoronaStats> call = coronaAPI.getGlobalStats("stats");
        call.enqueue(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onResponse(Call<CoronaStats> call, Response<CoronaStats> response) {
        if(response.isSuccessful()){
            coronaStats = response.body();
            if(fragment != null){
                fragment.makeChart(coronaStats);
            }
        }else{
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<CoronaStats> call, Throwable t) {
        t.printStackTrace();
    }

    public CoronaStats getCoronaStats() {
        return coronaStats;
    }
}
