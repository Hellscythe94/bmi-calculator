package wolowiec.michal.lab1.View.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wolowiec.michal.lab1.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CoronaRecepieFragment extends Fragment {

    public CoronaRecepieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_corona_recepie, container, false);
    }
}
