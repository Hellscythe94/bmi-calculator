package wolowiec.michal.lab1.View.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import wolowiec.michal.lab1.R;
import wolowiec.michal.lab1.View.Fragments.ChartFragment;
import wolowiec.michal.lab1.View.Fragments.CoronaRecepieFragment;

public class FragmentCntActivity extends AppCompatActivity {

    public final static int CHART_FRAGMENT = 1;
    public final static int RECIPE_FRAGMENT = 2;

    private Fragment fragment;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_cnt);

        Intent intent = getIntent();
        int fragmentType = intent.getIntExtra("fragment", CHART_FRAGMENT);

       switch (fragmentType){
           case CHART_FRAGMENT: {
               fragment = new ChartFragment();
               break;
           }

           case RECIPE_FRAGMENT: {
               fragment = new CoronaRecepieFragment();
               break;
           }
       }

        fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainContainer, fragment);
        fragmentTransaction.commit();
    }
}
