package wolowiec.michal.lab1.View.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wolowiec.michal.lab1.R;
import wolowiec.michal.lab1.View.Interfaces.OnFragmentInteractionListener;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.heightEditText)
    AppCompatEditText heightEditText;
    @BindView(R.id.weightEditText)
    AppCompatEditText weightEditText;
    @BindView(R.id.ageEditText)
    EditText ageEditText;
    @BindView(R.id.sexSpinner)
    Spinner sexSpinner;
    @BindView(R.id.calculateBmiBtn)
    AppCompatButton calculateBmiBtn;
    @BindView(R.id.bmiTextView)
    TextView bmiTextView;
    @BindView(R.id.caloriesTextView)
    TextView caloriesTextView;
    @BindView(R.id.recipeBtn)
    AppCompatButton recipeBtn;
    @BindView(R.id.chartBtn)
    AppCompatButton chartBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ArrayAdapter<CharSequence> sexSpinnerAdapter = ArrayAdapter.createFromResource(this,R.array.sexSpinner,R.layout.support_simple_spinner_dropdown_item);
        sexSpinner.setAdapter(sexSpinnerAdapter);
    }

    @OnClick(R.id.calculateBmiBtn)
    public void calculateBmiBtnClick(){
        if(weightEditText.getText().toString().equals("") || heightEditText.getText().toString().equals("")|| ageEditText.getText().toString().equals("")){
            Toast.makeText(this,"Please fill the form!", Toast.LENGTH_SHORT).show();
        } else{
            int weight = Integer.parseInt(weightEditText.getText().toString());
            double height = Integer.parseInt(heightEditText.getText().toString()) * 0.01;
            int age = Integer.parseInt(ageEditText.getText().toString());
            String sex = sexSpinner.getSelectedItem().toString();

            double bmi = weight/(height*height);

            if(bmi <= 18.5) bmiTextView.setText(String.format("Your BMI is: %.2f you are Underweight!", bmi));
            else if(bmi <= 24.9) bmiTextView.setText(String.format("Your BMI is: %.2f you are Normal weight!", bmi));
                else if(bmi <= 29.9) bmiTextView.setText(String.format("Your BMI is: %.2f you are Overweight!", bmi));
                    else bmiTextView.setText(String.format("Your BMI is: %.2f you are Obese!", bmi));

            int calorieInteke = 0;
            if(sex.equals("Male")) calorieInteke = (int) (66.5 + (weight * 13.75) + (5.003 * height / 0.01) - (6.775 * age));
            else calorieInteke = (int) (655.1 + (weight * 9.563) + (1.85 * height) - (4.676 * age));

            Resources res = getResources();

            caloriesTextView.setText(res.getString(R.string.caloriesTextViewText, calorieInteke));
        }
    }

    @OnClick(R.id.chartBtn)
    public void chartBtnClick(){
        Intent intent = new Intent(this, FragmentCntActivity.class);
        intent.putExtra("fragment", FragmentCntActivity.CHART_FRAGMENT);
        startActivity(intent);
    }

    @OnClick(R.id.recipeBtn)
    public void recipeBtnClick(){
        Intent intent = new Intent(this, FragmentCntActivity.class);
        intent.putExtra("fragment", FragmentCntActivity.RECIPE_FRAGMENT);
        startActivity(intent);
    }
}