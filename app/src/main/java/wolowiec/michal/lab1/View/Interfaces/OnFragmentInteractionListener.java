package wolowiec.michal.lab1.View.Interfaces;

import androidx.fragment.app.Fragment;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Fragment fragment);
}
