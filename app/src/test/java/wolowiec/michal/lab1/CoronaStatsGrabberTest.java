package wolowiec.michal.lab1;

import org.junit.Test;

import wolowiec.michal.lab1.Model.DataGrabbers.Retrofit.CoronaStatsGrabber;

import static org.junit.Assert.*;

public class CoronaStatsGrabberTest {

    @Test
    public void GrabberTest(){
        CoronaStatsGrabber coronaStatsGrabber = new CoronaStatsGrabber();

        coronaStatsGrabber.start();

        assertEquals((long)coronaStatsGrabber.getCoronaStats().getResults().get(0).getTotalAffectedCountries(), 204);
    }
}
