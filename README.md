# BMI Calculator

LAB1. BMI calculator for PAMO  
LAB2. Calorie calculator and COVID-19 recepie  
LAB3. COVID-19 Pie chart using an [API](https://api.thevirustracker.com/free-api?global=stats)

Used  
[ButterKnife](https://jakewharton.github.io/butterknife/) for binding Views.  
[MPAndroidChart](https://github.com/PhilJay/MPAndroidChart) for COVID-19 chart.  
[LOMBOK](https://projectlombok.org/) for code generation.  
[Retrofit](https://square.github.io/retrofit/) as a API client  